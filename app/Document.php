<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Document extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        "slug",
        "event_id",
        "type",
        "no_surat",
        "tanggal",
        "docx_path",
        "pdf_path",
        "document_data"
    ];

   
}
