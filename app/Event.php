<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Event extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        "slug",
        "nama_kegiatan",
        "tipe_pekerjaan",
        "daerah_kegiatan",
        "kode_mak",
        "tanggal_mulai",
        "tanggal_selesai",
        "uraian_pekerjaan",
        "jumlah_peserta_single_bed",
        "jumlah_hari_peserta_single_bed",
        "harga_satuan_single_bed",
        "harga_satuan_single_bed_text",
        "jumlah_harga_single_bed",
        "jumlah_peserta_twin_bed",
        "jumlah_hari_peserta_twin_bed",
        "harga_satuan_twin_bed",
        "harga_satuan_twin_bed_text",
        "jumlah_harga_twin_bed",
        "jumlah_peserta",
        "jumlah_hari_peserta",
        "harga_satuan",
        "harga_satuan_text",
        "jumlah_harga",
        "nilai_hps",
        "nilai_hps_text",
        "nilai_hps_terbilang",
        "nama_hotel",
        "alamat_hotel",
        "nama_perusahaan",
        "no_npwp_perusahaan",
        "nama_pejabat_hotel",
        "jabatan_pejabat_hotel",
        "nama_bank_perusahaan",
        "no_rekening_bank_perusahaan",
        "nama_pemilik_rekening_bank_perusahaan",
    ];

    public static function calculateHPS(FormRequest $r){
        $nilaiHps = 0;
        if($r->tipe_pekerjaan == "fullboard"){
            $totalSingle = $r->jumlah_peserta_single_bed *  $r->jumlah_hari_peserta_single_bed *  $r->harga_satuan_single_bed;
            $totalTwin = $r->jumlah_peserta_twin_bed *  $r->jumlah_hari_peserta_twin_bed *  $r->harga_satuan_twin_bed;
            $nilaiHps = $totalSingle + $totalTwin;

            return [$nilaiHps, $totalSingle, $totalTwin];
        }else{
            $nilaiHps = 
            $r->jumlah_peserta *
            $r->jumlah_hari_peserta * $r->harga_satuan;
        }

        return [$nilaiHps];
    }


    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }
}
