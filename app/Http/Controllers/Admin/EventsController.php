<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEventRequest;
use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Event;
use App\Document;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use PDF;
use NcJoes\OfficeConverter\OfficeConverter;


class EventsController extends Controller
{
    public function __construct(){
        $this->uraianKegiatan = [
            'fullboard' => "Fullboard (Konsumsi, Akomodasi, dan Ruang Sidang)",
            'fullday'   => "Fullday (Konsumsi, dan Ruang Sidang)",
            'halfday'   => "Halfday"
        ];
    }
    public function index()
    {
        abort_unless(\Gate::allows('event_access'), 403);

        $events = Event::all();
        $data = [
            "events" => $events,
            "breadcrumbs" => [trans('global.event.title_singular'), trans('global.list')]
        ];
        return view('admin.events.index', $data );
    }

    public function create()
    {
        abort_unless(\Gate::allows('event_create'), 403);

        return view('admin.events.create');
    }

    public function store(StoreEventRequest $request)
    {
        abort_unless(\Gate::allows('event_create'), 403);

        $request->merge(["nilai_hps" => Event::calculateHPS($request)[0]]);
        if($request->tipe_pekerjaan == "fullboard"){
            $request->merge(["jumlah_harga_single_bed" => str_replace("Rp. ","", $request->jumlah_harga_single_bed)]);
            $request->merge(["jumlah_harga_twin_bed" => str_replace("Rp. ","", $request->jumlah_harga_twin_bed)]);
        }else{
            $request->merge(["jumlah_harga" => str_replace("Rp. ","", $request->jumlah_harga)]);
        }
        $request->merge(["slug" => Str::slug($request->nama_kegiatan)."-".time()]);

        $event = Event::create($request->all());

        return redirect()->route('admin.events.index');
    }
    
    public function edit(Event $event)
    {
        abort_unless(\Gate::allows('event_edit'), 403);

        return view('admin.events.edit', compact('event'));
    }

    public function update(UpdateEventRequest $request, Event $event)
    {
        abort_unless(\Gate::allows('event_edit'), 403);

        $request->merge(["nilai_hps" => Event::calculateHPS($request)[0]]);
        if($request->tipe_pekerjaan == "fullboard"){
            $request->merge(["jumlah_harga_single_bed" => str_replace("Rp. ","", $request->jumlah_harga_single_bed)]);
            $request->merge(["jumlah_harga_twin_bed" => str_replace("Rp. ","", $request->jumlah_harga_twin_bed)]);
        }else{
            $request->merge(["jumlah_harga" => str_replace("Rp. ","", $request->jumlah_harga)]);
        }
        $request->merge(["slug" => Str::slug($request->nama_kegiatan)."-".time()]);

        $event->update($request->all());

        return redirect()->route('admin.events.index');
    }

    public function show(Event $event)
    {
        abort_unless(\Gate::allows('event_show'), 403);

        return view('admin.events.show', compact('event'));
    }

    public function destroy(Event $event)
    {
        abort_unless(\Gate::allows('event_delete'), 403);

        $event->delete();

        return back();
    }

    public function massDestroy(MassDestroyEventRequest $request)
    {
        Event::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }

    public function documents($id)
    {
        abort_unless(\Gate::allows('event_show'), 403);

        $event = Event::findOrFail($id);

        return view('admin.events.documents', compact('event'));
    }

    public function createDocument($eventId, $documentType){
        $event = Event::findOrFail($eventId);
        $document = $event->documents->where("type", $documentType)->first();
        // if($document->document_data != null){
        //     $document->document_data = json_decode($document->document_data);
        //     $obj = json_decode($document->document_data);
        // }
        $data = [
            "type" => $documentType,
            "event" => $event,
            "document" => $document,
            "document_data" =>$document != null && $document->document_data != null && $document->document_data != "" ? json_decode($document->document_data) : null,
        ];
        switch($documentType){
            case "cover":
                $view = 'admin.events.documents.cover.create';
                break;
            case "hps":
                $view = 'admin.events.documents.hps.create';
                break;
            case "permohonan":
                $view = 'admin.events.documents.permohonan.create';
                break;
            case "undanganpenyedia":
                $view = 'admin.events.documents.undanganpenyedia.create';
                break;
            default:
                abort(404);
                break;
        }

        return view($view, $data);
    }


    public function storeDocument(Request $r){

        $doc = Document::where([["event_id",$r->event_id], ["type", $r->type]])->first();

        $req = $this->prepareDocumentRequest($r);

        if($doc == null){
            $doc = Document::create($req->all());
        }
        else{
            $doc->update($req->all());
        }

        if( $doc->document_data != "" ||  $doc->document_data != null) {
            $doc->document_data = json_decode($doc->document_data);
        }

        if($doc !== null && $this->generateDocxFile($req, $doc)){
            return redirect()->route('admin.events.documents', [$r->event_id]);
        }else{
            abort(403);
        }
    }

    private function prepareDocumentRequest(Request $r){
        switch($r->type){
            case "cover":
                break;
            case "hps":
                $r->tanggal = $r->tanggal_hps;
                $r->merge(["tanggal" => $r->tanggal_hps]);
                $dataJson = [
                    "tanggal_hps" => $r->tanggal_hps
                ];
                $r->merge(["document_data" => json_encode($dataJson)]);
            case "permohonan":
                $dataJson = [
                    "tanggal" => $r->tanggal,
                    "no_surat" => $r->no_surat,
                ];
                $r->merge(["document_data" => json_encode($dataJson)]);
                break;
            case "undanganpenyedia":
                $dataJson = [
                    "tanggal" => $r->tanggal,
                    "no_surat" => $r->no_surat,
                    "tanggal_pemasukan_dokumen_penawaran" => $r->tanggal_pemasukan_dokumen_penawaran,
                    "waktu_pemasukan_dokumen_penawaran" => $r->waktu_pemasukan_dokumen_penawaran,
                    "tanggal_evaluasi_penawaran" => $r->tanggal_evaluasi_penawaran,
                    "waktu_evaluasi_penawaran" => $r->waktu_evaluasi_penawaran,
                    "tanggal_penetapan_pemenang" => $r->tanggal_penetapan_pemenang,
                    "waktu_penetapan_pemenang" => $r->waktu_penetapan_pemenang,
                    "tanggal_spk" => $r->tanggal_spk,
                    "tanggal_spmk" => $r->tanggal_spmk,
                ];
                $r->merge(["document_data" => json_encode($dataJson)]);
                break;
        }

        return $r;
    }

    private function generateDocxFile(Request $r, Document $doc){
        switch($r->type){
            case "cover":
                return $this->generateCoverDocxFile($r, $doc);
                break;
            case "hps":
                return $this->generateHPSDocxFile($r, $doc);
                break;
            case "permohonan":
                return $this->generatePermohonanDocxFile($r, $doc);
                break;
            case "undanganpenyedia":
                return $this->generateUndanganKePenyediaDocxFile($r, $doc);
                break;
        }
    }

    private function generateCoverDocxFile(Request $r, Document $doc){
        $event = Event::findOrFail($r->event_id);

        $templateName = '00.Cover_template';
        $templatePath = resource_path() .'/surat_template/'.$templateName.'.docx';
        $resultFolder = '/documents/';
        $resultName = "00.Cover";
        $resultFullName = $resultName."_".$event->slug;
        $resultPath = $resultFolder.$resultFullName;


        $tanggalMulai = $event->tanggal_mulai;
        $tanggalSelesai = $event->tanggal_selesai;
        if($event->tipe_pekerjaan == "fullboard"){
            $tanggalMulaiDD = explode("-", $tanggalMulai)[2];
            $periodeKegiatan = $tanggalMulaiDD.'-'.$this->tglIndo($tanggalSelesai);
        }else{
            $periodeKegiatan = $this->tglIndo($tanggalMulai);
        }

        // generate file
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        $templateProcessor->setValue('namaKegiatan', $event->nama_kegiatan);
        $templateProcessor->setValue('namaHotel', $event->nama_hotel);
        $templateProcessor->setValue('periodeKegiatan', $periodeKegiatan);

        return $this->_generateDocx($templateProcessor, $doc, $resultFolder, $resultPath, $resultFullName);
    }

    private function generateHPSDocxFile(Request $r, Document $doc){
        $event = Event::findOrFail($r->event_id);

        $templateName = '00.HPS_template';
        $templatePath = resource_path() .'/surat_template/'.$templateName.'.docx';
        $resultFolder = '/documents/';
        $resultName = "00.HPS";
        $resultFullName = $resultName."_".$event->slug;
        $resultPath = $resultFolder.$resultFullName;


        $tanggalMulai = $event->tanggal_mulai;
        $tanggalSelesai = $event->tanggal_selesai;
        if($event->tipe_pekerjaan == "fullboard"){
            $tanggalMulaiDD = explode("-", $tanggalMulai)[2];
            $periodeKegiatan = $tanggalMulaiDD.' s.d. '.$this->tglIndo($tanggalSelesai);

            if($event->jumlah_peserta_single_bed > 0 && $event->jumlah_peserta_twin_bed > 0){
                $rowsData = [
                    ["no"=>1,"periode" => $periodeKegiatan, 'uraian_pekerjaan' => $this->uraianKegiatan['fullboard'], 'jumlah_peserta' => $event->jumlah_peserta_single_bed." Orang Single Bed", 'jumlah_hari' => $event->jumlah_hari_peserta_single_bed, 'harga_satuan' => $event->harga_satuan_single_bed_text, 'jumlah_harga' => $event->jumlah_harga_single_bed],
                    ["no"=>2,"periode" => $periodeKegiatan, 'uraian_pekerjaan' => $this->uraianKegiatan['fullboard'], 'jumlah_peserta' => $event->jumlah_peserta_twin_bed." Orang Twin Bed", 'jumlah_hari' => $event->jumlah_hari_peserta_twin_bed, 'harga_satuan' => $event->harga_satuan_twin_bed_text, 'jumlah_harga' => $event->jumlah_harga_twin_bed],
                ];
            }else if($event->jumlah_peserta_single_bed > 0 && $event->jumlah_peserta_twin_bed < 1){
                $rowsData = [
                    ["no"=>1,"periode" => $periodeKegiatan, 'uraian_pekerjaan' => $this->uraianKegiatan['fullboard'], 'jumlah_peserta' => $event->jumlah_peserta_single_bed." Orang Single Bed", 'jumlah_hari' => $event->jumlah_hari_peserta_single_bed, 'harga_satuan' => $event->harga_satuan_single_bed_text, 'jumlah_harga' => $event->jumlah_harga_single_bed],
                ];
            }else{
                $rowsData = [
                    ["no"=>1,"periode" => $periodeKegiatan, 'uraian_pekerjaan' => $this->uraianKegiatan['fullboard'], 'jumlah_peserta' => $event->jumlah_peserta_twin_bed." Orang Twin Bed", 'jumlah_hari' => $event->jumlah_hari_peserta_twin_bed, 'harga_satuan' => $event->harga_satuan_twin_bed_text, 'jumlah_harga' => $event->jumlah_harga_twin_bed],
                ];
            }
        }else{
            $periodeKegiatan = $this->tglIndo($tanggalMulai);
            
            $rowsData = [
                ["no"=>1,"periode" => $periodeKegiatan, 'uraian_pekerjaan' => $event->tipe_pekerjaan == "fullday" ? $this->uraianKegiatan['fullday'] : $this->uraianKegiatan['halfday'], 'jumlah_peserta' => $event->jumlah_peserta." Orang", 'jumlah_hari' => $event->jumlah_hari_peserta, 'harga_satuan' => $event->harga_satuan_text, 'jumlah_harga' => $event->jumlah_harga],
            ];
        }

        // generate file
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        $templateProcessor->setValue('nama_kegiatan', Str::upper($event->nama_kegiatan));
        $templateProcessor->setValue('nilai_hps', $event->nilai_hps_text);
        $templateProcessor->setValue('nilai_hps_terbilang', $event->nilai_hps_terbilang);
        $templateProcessor->setValue('tanggal_titimangsa', $this->tglIndo($doc->tanggal));

        $templateProcessor->cloneRowAndSetValues("no",$rowsData);

        return $this->_generateDocx($templateProcessor, $doc, $resultFolder, $resultPath, $resultFullName);
    }

    private function generatePermohonanDocxFile(Request $r, Document $doc){
        $event = Event::findOrFail($r->event_id);

        $templateName = '01.Permohonan_template';
        $templatePath = resource_path() .'/surat_template/'.$templateName.'.docx';
        $resultFolder = '/documents/';
        $resultName = "01.Permohoan";
        $resultFullName = $resultName."_".$event->slug;
        $resultPath = $resultFolder.$resultFullName;


        $tanggalMulai = $event->tanggal_mulai;
        $tanggalSelesai = $event->tanggal_selesai;
        if($event->tipe_pekerjaan == "fullboard"){
            $tanggalMulaiDD = explode("-", $tanggalMulai)[2];
            $periodeKegiatan = $tanggalMulaiDD.' s.d. '.$this->tglIndo($tanggalSelesai);
        }else{
            $periodeKegiatan = $this->tglIndo($tanggalMulai);
        }

        // generate file
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        $templateProcessor->setValue('no_surat', $doc->no_surat);
        $templateProcessor->setValue('nama_kegiatan', $event->nama_kegiatan);
        $templateProcessor->setValue('periode', $periodeKegiatan);
        $templateProcessor->setValue('daerah_kegiatan', $event->daerah_kegiatan);
        $templateProcessor->setValue('nilai_hps', $event->nilai_hps_text);
        $templateProcessor->setValue('nilai_pagu', $event->nilai_hps_text);
        $templateProcessor->setValue('kode_mak', $event->kode_mak);
        $templateProcessor->setValue('tanggal', $this->tglIndo($doc->tanggal));

        return $this->_generateDocx($templateProcessor, $doc, $resultFolder, $resultPath, $resultFullName);
    }

    private function generateUndanganKePenyediaDocxFile(Request $r, Document $doc){
        $event = Event::findOrFail($r->event_id);

        $templateName = '02. Undangan ke Penyedia_template';
        $templatePath = resource_path() .'/surat_template/'.$templateName.'.docx';
        $resultFolder = '/documents/';
        $resultName = "02. Undangan ke Penyedia";
        $resultFullName = $resultName."_".$event->slug;
        $resultPath = $resultFolder.$resultFullName;


        $tanggalMulai = $event->tanggal_mulai;
        $tanggalSelesai = $event->tanggal_selesai;
        if($event->tipe_pekerjaan == "fullboard"){
            $tanggalMulaiDD = explode("-", $tanggalMulai)[2];
            $periodeKegiatan = $tanggalMulaiDD.' s.d. '.$this->tglIndo($tanggalSelesai);
        }else{
            $periodeKegiatan = $this->tglIndo($tanggalMulai);
        }

        // generate file
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($templatePath);

        $templateProcessor->setValue('no_surat', $doc->no_surat);
        $templateProcessor->setValue('nama_kegiatan', $event->nama_kegiatan);
        $templateProcessor->setValue('nama_perusahaan', $event->nama_perusahaan);
        $templateProcessor->setValue('nama_hotel', $event->nama_hotel);
        $templateProcessor->setValue('alamat_hotel', $event->alamat_hotel);
        $templateProcessor->setValue('nilai_hps', $event->nilai_hps_text);
        $templateProcessor->setValue('tanggal', $this->tglIndo($doc->tanggal));
        $templateProcessor->setValue('tanggal_pemasukan_dokumen_penawaran', $this->formatHariTanggal($r->tanggal_pemasukan_dokumen_penawaran));
        $templateProcessor->setValue('tanggal_evaluasi_penawaran', $this->formatHariTanggal($r->tanggal_evaluasi_penawaran));
        $templateProcessor->setValue('tanggal_penetapan_pemenang', $this->formatHariTanggal($r->tanggal_penetapan_pemenang));
        $templateProcessor->setValue('tanggal_spk', $this->formatHariTanggal($r->tanggal_spk));
        $templateProcessor->setValue('tanggal_spmk', $this->formatHariTanggal($r->tanggal_spmk));
        $templateProcessor->setValue('waktu_pemasukan_dokumen_penawaran', $r->waktu_pemasukan_dokumen_penawaran);
        $templateProcessor->setValue('waktu_evaluasi_penawaran', $r->waktu_evaluasi_penawaran);
        $templateProcessor->setValue('waktu_penetapan_pemenang', $r->waktu_penetapan_pemenang);

        return $this->_generateDocx($templateProcessor, $doc, $resultFolder, $resultPath, $resultFullName);
    }

    private function _generateDocx(\PhpOffice\PhpWord\TemplateProcessor $templateProcessor, Document $doc, $resultFolder, $resultPath, $resultFullName ){
        $resultDocxPath = $resultPath.'.docx';
        if(!file_exists(public_path().$resultFolder)){
            mkdir(public_path().$resultFolder, 0755, true);
        }
        $templateProcessor->saveAs(public_path().$resultDocxPath);

        
        if(file_exists(public_path().$resultDocxPath)){
            if($doc->document_data) $doc->document_data = json_encode($doc->document_data);
            $doc->update(["docx_path" => $resultDocxPath]);
            $this->convertDocToPDF($resultDocxPath, $resultFolder, $resultFullName, $doc);
            return true;
        }else{
            return false;
        }
    }

    private function tglIndo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tahun
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tanggal
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    private function formatHariTanggal($waktu)
    {
        $hari_array = array(
            'Minggu',
            'Senin',
            'Selasa',
            'Rabu',
            'Kamis',
            'Jumat',
            'Sabtu'
        );
        $hr = date('w', strtotime($waktu));
        $hari = $hari_array[$hr];
        $tanggal = date('j', strtotime($waktu));
        $bulan_array = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        );
        $bl = date('n', strtotime($waktu));
        $bulan = $bulan_array[$bl];
        $tahun = date('Y', strtotime($waktu));
        $jam = date( 'H:i:s', strtotime($waktu));
        
        //untuk menampilkan hari, tanggal bulan tahun jam
        //return "$hari, $tanggal $bulan $tahun $jam";

        //untuk menampilkan hari, tanggal bulan tahun
        return "$hari, $tanggal $bulan $tahun";
    }

    private function convertDocToPDF($docxPath,$resultFolder, $resultFullName,  Document $doc){
        try{
            if(config('app.env') == "production"){
                $converter = new OfficeConverter( public_path().$docxPath);
            }else{
                $converter = new OfficeConverter( public_path().$docxPath, null, '/Applications/LibreOffice.app/Contents/MacOS/soffice', true );
            }
            $converter->convertTo($resultFullName.".pdf");
            if(file_exists(public_path().$resultFolder.$resultFullName.".pdf")){
                if($doc->document_data) $doc->document_data = json_encode($doc->document_data);
                $doc->update(["pdf_path" => $resultFolder.$resultFullName.".pdf"]);
            }
        }catch(NcJoes\OfficeConverter\OfficeConverterException $error){
            echo "aaa";
        }

   }
}
