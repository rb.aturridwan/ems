<?php

namespace App\Http\Controllers\Admin;
use App\Event;

class HomeController
{
    public function index()
    {
        $data = [
            "breadcrumbs" => ["Dashboard"],
            "countEvents" => Event::count()
        ];

        return view('home', $data );
    }
}
