$(document).ready(function () {
  window._token = $('meta[name="csrf-token"]').attr('content')

  ClassicEditor.create(document.querySelector('.ckeditor'))

  moment.updateLocale('en', {
    week: {dow: 1} // Monday is the first day of the week
  })

  $('.date').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: 'en'
  })

  $('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    locale: 'en',
    sideBySide: true
  })

  $('.timepicker').datetimepicker({
    format: 'HH:mm:ss'
  })

  $('.select-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', 'selected')
    $select2.trigger('change')
  })
  $('.deselect-all').click(function () {
    let $select2 = $(this).parent().siblings('.select2')
    $select2.find('option').prop('selected', '')
    $select2.trigger('change')
  })

  $('.select2').select2()

  $('.treeview').each(function () {
    var shouldExpand = false
    $(this).find('li').each(function () {
      if ($(this).hasClass('active')) {
        shouldExpand = true
      }
    })
    if (shouldExpand) {
      $(this).addClass('active')
    }
  })
})

var ems = {
    formatMoneyToText:function(value, currency=""){
      if( isNaN(value)) return "";
      return (ems.pembilang(value) + ""+ currency).replace(/^\s+|\s+$/gm,'');
    },
    pembilang: function(nilai){
        nilai = Math.abs(nilai);
        var simpanNilaiBagi=0;
        var huruf = ["", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"];
        var temp="";
     
        if (nilai < 12) {
            temp = " "+huruf[nilai];
        }
        else if (nilai <20) {
            temp = this.pembilang(nilai - 10) + " belas";
        }
        else if (nilai < 100) {
            simpanNilaiBagi = Math.floor(nilai/10);
            temp = this.pembilang(simpanNilaiBagi)+" puluh"+ this.pembilang(nilai % 10);
        }
        else if (nilai < 200) {
            temp = " Seratus" + this.pembilang(nilai - 100);
        }
        else if (nilai < 1000) {
            simpanNilaiBagi = Math.floor(nilai/100);
            temp = this.pembilang(simpanNilaiBagi) + " ratus" + this.pembilang(nilai % 100);
        }
         else if (nilai < 2000) {
            temp = " Seribu" + this.pembilang(nilai - 1000);
        }
        else if (nilai < 1000000) {
            simpanNilaiBagi = Math.floor(nilai/1000);
            temp = this.pembilang(simpanNilaiBagi) + " ribu" + this.pembilang(nilai % 1000);
        } 
        else if (nilai < 1000000000) {
            simpanNilaiBagi = Math.floor(nilai/1000000);
            temp =this.pembilang(simpanNilaiBagi) + " juta" + this.pembilang(nilai % 1000000);
        } 
        else if (nilai < 1000000000000) {
            simpanNilaiBagi = Math.floor(nilai/1000000000);
            temp = this.pembilang(simpanNilaiBagi) + " miliar" + this.pembilang(nilai % 1000000000);
        } 
        else if (nilai < 1000000000000000) {
            simpanNilaiBagi = Math.floor(nilai/1000000000000);
            temp = this.pembilang(nilai/1000000000000) + " triliun" + this.pembilang(nilai % 1000000000000);
        }
     
        return temp.replace(/ +(?= )/g,'');
    },
    toSentenceCase: function(str){
      console.log(str.toLowerCase().charAt(0).toUpperCase())
      return str.toLowerCase().charAt(0).toUpperCase() + str.slice(1);
  },
  dateDiff: function(val1, val2) {
  
    var t2 = new Date(val1);
    var t1 = new Date(val2);
  
    return ((t1 - t2) / (24 * 3600 * 1000));
  }
}

function clone(obj) {
  if (null == obj || "object" != typeof obj) return obj;
  var copy = obj.constructor();
  for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
  }
  return copy;
}