<?php

use App\Event;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EventTableSeeder extends Seeder
{
    public function run()
    {
        $event = [[
            'id'            => 1,
            'slug'          => Str::slug("Pengadaan Paket Fullboard Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023"),
            'nama_kegiatan'         => "Pengadaan Paket Fullboard Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023",
            'tipe_pekerjaan'            => "fullboard",
            'daerah_kegiatan'           => "Kemayoran",
            'kode_mak'          => "1234567890",
            'tanggal_mulai'         => "2023-03-26",
            'tanggal_selesai'           => "2023-03-28",
            'uraian_pekerjaan'          => null,

            'jumlah_peserta_single_bed'         => "10",
            'jumlah_hari_peserta_single_bed'            => "2",
            'harga_satuan_single_bed'           => 1000000,
            'harga_satuan_single_bed_text'           => "1.000.000",
            'jumlah_harga_single_bed'           => "20.000.000",

            'jumlah_peserta_twin_bed'           => "10",
            'jumlah_hari_peserta_twin_bed'          => "2",
            'harga_satuan_twin_bed'         => "1000000",
            'harga_satuan_twin_bed_text'         => "1.000.000",
            'jumlah_harga_twin_bed'         => "20.000.000",
            'jumlah_peserta'            => null,
            'jumlah_hari_peserta'           => "2",
            'harga_satuan'          => null,
            'harga_satuan_text'          => null,
            'jumlah_harga'          => null,
            'nilai_hps'         => "40000000",
            'nilai_hps_text'         => "40.000.000",
            'nilai_hps_terbilang'         => "Empat puluh juta rupiah",
            'nama_hotel'            => "Golden Boutique Hotel Kemayoran",
            'alamat_hotel'          => "Kemayoran",
            'nama_perusahaan'           => "Golden Boutique Hotel Kemayoran",
            'no_npwp_perusahaan'            => "0123456789",
            'nama_pejabat_hotel'            => "John Doe",
            'jabatan_pejabat_hotel'         => "Marketing",
            'nama_bank_perusahaan'          => "BCA",
            'no_rekening_bank_perusahaan'           => "0123456789",
            'nama_pemilik_rekening_bank_perusahaan'         => "Pemilik",
            'status'            => "inprogress",
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ]];

        Event::insert($event);

        $event = [[
            'id'            => 2,
            'slug'          => Str::slug("Pengadaan Paket Fullday Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023"),
            'nama_kegiatan'         => "Pengadaan Paket Fullday Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023",
            'tipe_pekerjaan'            => "fullday",
            'daerah_kegiatan'           => "Kemayoran",
            'kode_mak'          => "1234567890",
            'tanggal_mulai'         => "2023-03-26",
            'tanggal_selesai'           => "2023-03-28",
            'uraian_pekerjaan'          => null,

            'jumlah_peserta_single_bed'         => null,
            'jumlah_hari_peserta_single_bed'            => "1",
            'harga_satuan_single_bed'           => null,
            'harga_satuan_single_bed_text'           => null,
            'jumlah_harga_single_bed'           => null,

            'jumlah_peserta_twin_bed'           => null,
            'jumlah_hari_peserta_twin_bed'          => "1",
            'harga_satuan_twin_bed'         => null,
            'harga_satuan_twin_bed_text'         => null,
            'jumlah_harga_twin_bed'         => null,

            'jumlah_peserta'            => 2,
            'jumlah_hari_peserta'           => "1",
            'harga_satuan'          => 2000000,
            'harga_satuan_text'          => "2.000.000",
            'jumlah_harga'          => "4.000.000",
            'nilai_hps'         => "4000000",
            'nilai_hps_text'         => "4.000.000",
            'nilai_hps_terbilang'         => "Empat juta rupiah",
            'nama_hotel'            => "Golden Boutique Hotel Kemayoran",
            'alamat_hotel'          => "Kemayoran",
            'nama_perusahaan'           => "Golden Boutique Hotel Kemayoran",
            'no_npwp_perusahaan'            => "0123456789",
            'nama_pejabat_hotel'            => "John Doe",
            'jabatan_pejabat_hotel'         => "Marketing",
            'nama_bank_perusahaan'          => "BCA",
            'no_rekening_bank_perusahaan'           => "0123456789",
            'nama_pemilik_rekening_bank_perusahaan'         => "Pemilik",
            'status'            => "inprogress",
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ]];

        Event::insert($event);

        $event = [[
            'id'            => 3,
            'slug'          => Str::slug("Pengadaan Paket Halfday Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023"),
            'nama_kegiatan'         => "Pengadaan Paket Halfday Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023",
            'tipe_pekerjaan'            => "halfday",
            'daerah_kegiatan'           => "Kemayoran",
            'kode_mak'          => "1234567890",
            'tanggal_mulai'         => "2023-03-26",
            'tanggal_selesai'           => "2023-03-28",
            'uraian_pekerjaan'          => null,

            'jumlah_peserta_single_bed'         => null,
            'jumlah_hari_peserta_single_bed'            => "1",
            'harga_satuan_single_bed'           => null,
            'harga_satuan_single_bed_text'           => null,
            'jumlah_harga_single_bed'           => null,

            'jumlah_peserta_twin_bed'           => null,
            'jumlah_hari_peserta_twin_bed'          => "1",
            'harga_satuan_twin_bed'         => null,
            'harga_satuan_twin_bed_text'         => null,
            'jumlah_harga_twin_bed'         => null,

            'jumlah_peserta'            => 2,
            'jumlah_hari_peserta'           => "1",
            'harga_satuan'          => 2000000,
            'harga_satuan_text'          => "2.000.000",
            'jumlah_harga'          => "4.000.000",
            'nilai_hps'         => "4000000",
            'nilai_hps_text'         => "4.000.000",
            'nilai_hps_terbilang'         => "Empat juta rupiah",
            'nama_hotel'            => "Golden Boutique Hotel Kemayoran",
            'alamat_hotel'          => "Kemayoran",
            'nama_perusahaan'           => "Golden Boutique Hotel Kemayoran",
            'no_npwp_perusahaan'            => "0123456789",
            'nama_pejabat_hotel'            => "John Doe",
            'jabatan_pejabat_hotel'         => "Marketing",
            'nama_bank_perusahaan'          => "BCA",
            'no_rekening_bank_perusahaan'           => "0123456789",
            'nama_pemilik_rekening_bank_perusahaan'         => "Pemilik",
            'status'            => "inprogress",
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ]];

        Event::insert($event);

        $event = [[
            'id'            => 4,
            'slug'          => Str::slug("Pengadaan Paket Fullboard Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023"),
            'nama_kegiatan'         => "Pengadaan Paket Fullboard Meeting Kegiatan Pembekalan Fasilitator Umpan Balik Capaian Pembelajaran dalam rangka kurikulum yang dikembangkan tahun 2023",
            'tipe_pekerjaan'            => "fullboard",
            'daerah_kegiatan'           => "Kemayoran",
            'kode_mak'          => "1234567890",
            'tanggal_mulai'         => "2023-03-26",
            'tanggal_selesai'           => "2023-03-28",
            'uraian_pekerjaan'          => null,

            'jumlah_peserta_single_bed'         => "20",
            'jumlah_hari_peserta_single_bed'            => "2",
            'harga_satuan_single_bed'           => 1000000,
            'harga_satuan_single_bed_text'           => "1.000.000",
            'jumlah_harga_single_bed'           => "40.000.000",

            'jumlah_peserta_twin_bed'           => null,
            'jumlah_hari_peserta_twin_bed'          => "1",
            'harga_satuan_twin_bed'         => null,
            'harga_satuan_twin_bed_text'         => null,
            'jumlah_harga_twin_bed'         => null,

            'jumlah_peserta'            => null,
            'jumlah_hari_peserta'           => "1",
            'harga_satuan'          => null,
            'harga_satuan_text'          => null,
            'jumlah_harga'          => null,
            'nilai_hps'         => "40000000",
            'nilai_hps_text'         => "40.000.000",
            'nilai_hps_terbilang'         => "Empat puluh juta rupiah",
            'nama_hotel'            => "Golden Boutique Hotel Kemayoran",
            'alamat_hotel'          => "Kemayoran",
            'nama_perusahaan'           => "Golden Boutique Hotel Kemayoran",
            'no_npwp_perusahaan'            => "0123456789",
            'nama_pejabat_hotel'            => "John Doe",
            'jabatan_pejabat_hotel'         => "Marketing",
            'nama_bank_perusahaan'          => "BCA",
            'no_rekening_bank_perusahaan'           => "0123456789",
            'nama_pemilik_rekening_bank_perusahaan'         => "Pemilik",
            'status'            => "inprogress",
            'created_at'     => '2019-04-15 19:13:32',
            'updated_at'     => '2019-04-15 19:13:32',
            'deleted_at'     => null,
        ]];

        Event::insert($event);
    }
}
