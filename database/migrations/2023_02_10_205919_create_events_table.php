<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string("slug");
            $table->string("nama_kegiatan");
            $table->string("tipe_pekerjaan");
            $table->string("daerah_kegiatan")->nullable();
            $table->string("kode_mak")->nullable();
            $table->date("tanggal_mulai")->nullable();
            $table->date("tanggal_selesai")->nullable();
            $table->string("uraian_pekerjaan")->nullable();

            $table->string("jumlah_peserta_single_bed")->nullable();
            $table->string("jumlah_hari_peserta_single_bed")->nullable();
            $table->double("harga_satuan_single_bed")->nullable();
            $table->string("harga_satuan_single_bed_text")->nullable();
            $table->string("jumlah_harga_single_bed")->nullable();

            $table->unsignedInteger("jumlah_peserta_twin_bed")->nullable();
            $table->unsignedInteger("jumlah_hari_peserta_twin_bed")->nullable();
            $table->double("harga_satuan_twin_bed")->nullable();
            $table->string("harga_satuan_twin_bed_text")->nullable();
            $table->string("jumlah_harga_twin_bed")->nullable();

            $table->unsignedInteger("jumlah_peserta")->nullable();
            $table->unsignedInteger("jumlah_hari_peserta")->nullable();
            $table->double("harga_satuan")->nullable();
            $table->string("harga_satuan_text")->nullable();
            $table->string("jumlah_harga")->nullable();

            $table->double("nilai_hps")->nullable();
            $table->string("nilai_hps_text")->nullable();
            $table->string("nilai_hps_terbilang")->nullable();

            $table->string("nama_hotel")->nullable();
            $table->string("alamat_hotel")->nullable();
            $table->string("nama_perusahaan")->nullable();
            $table->string("no_npwp_perusahaan")->nullable();
            $table->string("nama_pejabat_hotel")->nullable();
            $table->string("jabatan_pejabat_hotel")->nullable();
            $table->string("nama_bank_perusahaan")->nullable();
            $table->string("no_rekening_bank_perusahaan")->nullable();
            $table->string("nama_pemilik_rekening_bank_perusahaan")->nullable();
            $table->string("status")->default("inprogress");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
