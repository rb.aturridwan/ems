<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("nama_perusahaan");
            $table->string("no_npwp_perusahaan")->nullable();
            $table->string("nama_pejabat_hotel")->nullable();
            $table->string("jabatan_pejabat_hotel")->nullable();
            $table->string("nama_bank_perusahaan")->nullable();
            $table->string("no_rekening_bank_perusahaan")->nullable();
            $table->string("nama_pemilik_rekening_bank_perusahaan")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
