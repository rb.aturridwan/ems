@extends('layouts.admin')
@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ trans('global.event.title_singular') }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @isset($breadcrumbs)
                    @foreach($breadcrumbs as $key => $breadcrumb )
                        @if($key == count($breadcrumbs)-1)
                        <li class="breadcrumb-item active">{{$breadcrumb}}</li>
                        @else
                        <li class="breadcrumb-item">{{$breadcrumb}}</li>
                        @endif
                    @endforeach
                    @endisset
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>{{ trans('global.show') }} {{ trans('global.event.title') }}</h5>
                </div>
            
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th width="30%">
                                    {{ trans('global.event.fields.nama_kegiatan') }}
                                </th>
                                <td>
                                    {{ $event->nama_kegiatan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.tipe_pekerjaan') }}
                                </th>
                                <td>
                                    {!! $event->tipe_pekerjaan !!}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.tanggal_mulai') }}
                                </th>
                                <td>
                                    {{ $event->tanggal_mulai }}
                                </td>
                            </tr>
                            @if($event->tipe_pekerjaan == "fullboard")
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.tanggal_selesai') }}
                                </th>
                                <td>
                                    {{ $event->tanggal_selesai }}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Dokumen Kegiatan</h5>
                </div>
                <?php
                    function findObjectByType($value, $array){

                        foreach ( $array as $element ) {
                            if ( $value == $element->type ) {
                                return $element;
                            }
                        }
                        return null;
                    }

                    $cover = findObjectByType("cover", $event->documents);
                    $hps = findObjectByType("hps", $event->documents);
                    $permohonan = findObjectByType("permohonan", $event->documents);
                    $undanganpenyedia = findObjectByType("undanganpenyedia", $event->documents);
                ?>
            
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th width="30%" style="vertical-align: middle">
                                    00.Cover
                                </th>
                                <td style="vertical-align: middle">
                                    @if(isset($cover) && $cover->docx_path != "" && file_exists(public_path().$cover->docx_path))
                                    <a target="_blank" class="btn btn-primary float-right ml-2" href="{{asset($cover->docx_path)}}">Download Docx</a>
                                        @if($cover->pdf_path != "" && file_exists(public_path().$cover->pdf_path))
                                            <a target="_blank" class="btn btn-danger float-right ml-2" href="{{asset($cover->pdf_path)}}">Download PDF</a>
                                        @endif
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "cover"])}}>Edit & Buat Ulang Dokumen</a>
                                    {{-- <a class="btn btn-danger" href="{{asset($cover->docx_path)}}">Preview</a> --}}
                                    @else
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "cover"])}}>Buat Dokumen</a>
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th width="30%" style="vertical-align: middle">
                                    00.HPS
                                </th>
                                <td style="vertical-align: middle">
                                    @if(isset($hps) && $hps->docx_path != "" && file_exists(public_path().$hps->docx_path))
                                    <a target="_blank" class="btn btn-primary float-right ml-2" href="{{asset($hps->docx_path)}}">Download Docx</a>
                                    @if($hps->pdf_path != "" && file_exists(public_path().$hps->pdf_path))
                                        <a target="_blank" class="btn btn-danger float-right ml-2" href="{{asset($hps->pdf_path)}}">Download PDF</a>
                                    @endif
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "hps"])}}>Edit & Buat Ulang Dokumen</a>
                                    {{-- <a class="btn btn-danger" href="{{asset($hps->docx_path)}}">Preview</a> --}}
                                    @else
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "hps"])}}>Buat Dokumen</a>
                                    @endif
                                </td>
                            </tr>

                            <tr>
                                <th width="30%" style="vertical-align: middle">
                                    01.Permohonan
                                </th>
                                <td style="vertical-align: middle">
                                    @if(isset($permohonan) && $permohonan->docx_path != "" && file_exists(public_path().$permohonan->docx_path))
                                    <a target="_blank" class="btn btn-primary float-right ml-2" href="{{asset($permohonan->docx_path)}}">Download Docx</a>
                                    @if($permohonan->pdf_path != "" && file_exists(public_path().$permohonan->pdf_path))
                                        <a target="_blank" class="btn btn-danger float-right ml-2" href="{{asset($permohonan->pdf_path)}}">Download PDF</a>
                                    @endif
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "permohonan"])}}>Edit & Buat Ulang Dokumen</a>
                                    {{-- <a class="btn btn-danger" href="{{asset($permohonan->docx_path)}}">Preview</a> --}}
                                    @else
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "permohonan"])}}>Buat Dokumen</a>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th width="30%" style="vertical-align: middle">
                                    02.Undangan Ke Penyedia
                                </th>
                                <td style="vertical-align: middle">
                                    @if(isset($undanganpenyedia) && $undanganpenyedia->docx_path != "" && file_exists(public_path().$undanganpenyedia->docx_path))
                                    <a target="_blank" class="btn btn-primary float-right ml-2" href="{{asset($undanganpenyedia->docx_path)}}">Download Docx</a>
                                    @if($undanganpenyedia->pdf_path != "" && file_exists(public_path().$undanganpenyedia->pdf_path))
                                        <a target="_blank" class="btn btn-danger float-right ml-2" href="{{asset($undanganpenyedia->pdf_path)}}">Download PDF</a>
                                    @endif
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "undanganpenyedia"])}}>Edit & Buat Ulang Dokumen</a>
                                    {{-- <a class="btn btn-danger" href="{{asset($undanganpenyedia->docx_path)}}">Preview</a> --}}
                                    @else
                                    <a class="btn btn-success float-right" href={{route('admin.events.documents.create', [$event->id, "undanganpenyedia"])}}>Buat Dokumen</a>
                                    @endif
                                </td>
                            </tr>



                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection