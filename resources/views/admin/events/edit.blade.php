@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('global.event.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.events.update", [$event->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            {{-- main card --}}
            <div class="card">
                <div class="card-header">
                    <h4>Kegiatan</h4>
                </div>

                <div class="card-body">
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nama_kegiatan') ? 'has-error' : '' }}">
                                <label for="nama_kegiatan">{{ trans('global.event.fields.nama_kegiatan') }}<span class="text-danger">*</span></label>
                                <input type="text" id="nama_kegiatan" name="nama_kegiatan" class="form-control" value="{{ old('nama_kegiatan', isset($event) ? $event->nama_kegiatan : '') }}">
                                @if($errors->has('nama_kegiatan'))
                                    <p class="help-block">
                                        {{ $errors->first('nama_kegiatan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nama_kegiatan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('tipe_pekerjaan') ? 'has-error' : '' }}">
                                <label for="tipe_pekerjaan">{{ trans('global.event.fields.tipe_pekerjaan') }}<span class="text-danger">*</span></label>
                                <select type="text" id="tipe_pekerjaan" name="tipe_pekerjaan" class="form-control" value="{{ old('tipe_pekerjaan', isset($event) ? $event->tipe_pekerjaan : '') }}">
                                    <option></option>
                                    <option value="fullboard">Fullboard</option>
                                    <option value="fullday">Fullday</option>
                                    <option value="halfday">Halfday</option>
                                </select>
                                @if($errors->has('tipe_pekerjaan'))
                                    <p class="help-block">
                                        {{ $errors->first('tipe_pekerjaan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.tipe_pekerjaan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('kode_mak') ? 'has-error' : '' }}">
                                <label for="kode_mak">{{ trans('global.event.fields.kode_mak') }}</label>
                                <input type="text" id="daerah_kegiatan" name="kode_mak" class="form-control" value="{{ old('kode_mak', isset($event) ? $event->kode_mak : '') }}">
                                @if($errors->has('kode_mak'))
                                    <p class="help-block">
                                        {{ $errors->first('kode_mak') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.kode_mak_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('daerah_kegiatan') ? 'has-error' : '' }}">
                                <label for="daerah_kegiatan">{{ trans('global.event.fields.daerah_kegiatan') }}<span class="text-danger">*</span></label>
                                <input type="text" id="daerah_kegiatan" name="daerah_kegiatan" class="form-control" value="{{ old('daerah_kegiatan', isset($event) ? $event->daerah_kegiatan : '') }}">
                                @if($errors->has('daerah_kegiatan'))
                                    <p class="help-block">
                                        {{ $errors->first('daerah_kegiatan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.daerah_kegiatan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        
                    </div>
                    {{-- end row --}}
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('tanggal_mulai') ? 'has-error' : '' }}">
                                <label for="tanggal_mulai">{{ trans('global.event.fields.tanggal_mulai') }}<span class="text-danger">*</span></label>
                                <input type="date" id="tanggal_mulai" name="tanggal_mulai" class="form-control" value="{{ old('tanggal_mulai', isset($event) ? $event->tanggal_mulai : date('Y-m-d')) }}" >
                                @if($errors->has('tanggal_mulai'))
                                    <p class="help-block">
                                        {{ $errors->first('tanggal_mulai') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.tanggal_mulai_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12 only-fullboard" style="display:none">
                            <div class="form-group {{ $errors->has('tanggal_selesai') ? 'has-error' : '' }}">
                                <label for="tanggal_selesai">{{ trans('global.event.fields.tanggal_selesai') }}<span class="text-danger">*</span></label>
                                <input type="date" id="tanggal_selesai" name="tanggal_selesai" class="form-control" value="{{ old('tanggal_selesai', isset($event) ? $event->tanggal_selesai : date('Y-m-d', strtotime("+1 day"))) }}" >
                                @if($errors->has('tanggal_selesai'))
                                    <p class="help-block">
                                        {{ $errors->first('tanggal_selesai') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.tanggal_selesai_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}
                </div>
            </div>
            {{-- end main card --}}

            {{-- single bed card --}}
            <div class="card only-fullboard" style="display:none">
                <div class="card-header">
                    <h4>Peserta Single Bed</h4>
                </div>

                <div class="card-body">
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_peserta_single_bed') ? 'has-error' : '' }}">
                                <label for="jumlah_peserta_single_bed">{{ trans('global.event.fields.jumlah_peserta_single_bed') }}</label>
                                <input type="number" id="jumlah_peserta_single_bed" name="jumlah_peserta_single_bed" class="form-control" value="{{ old('jumlah_peserta_single_bed', isset($event) ? $event->jumlah_peserta_single_bed : '') }}">
                                @if($errors->has('jumlah_peserta_single_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_peserta_single_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_peserta_single_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_hari_peserta_single_bed') ? 'has-error' : '' }}">
                                <label for="jumlah_hari_peserta_single_bed">{{ trans('global.event.fields.jumlah_hari_peserta_single_bed') }}</label>
                                <input type="number" id="jumlah_hari_peserta_single_bed" name="jumlah_hari_peserta_single_bed" class="form-control" value="{{ old('jumlah_hari_peserta_single_bed', isset($event) ? $event->jumlah_hari_peserta_single_bed : 1) }}" readonly>
                                @if($errors->has('jumlah_hari_peserta_single_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_hari_peserta_single_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_hari_peserta_single_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}

                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('harga_satuan_single_bed') ? 'has-error' : '' }}">
                                <label for="harga_satuan_single_bed">{{ trans('global.event.fields.harga_satuan_single_bed') }}</label>
                                <input type="number" id="harga_satuan_single_bed" name="harga_satuan_single_bed" class="form-control" value="{{ old('harga_satuan_single_bed', isset($event) ? $event->harga_satuan_single_bed : '') }}">
                                <input type="hidden" id="harga_satuan_single_bed_text" name="harga_satuan_single_bed_text" class="form-control" value="{{ old('harga_satuan_single_bed_text', isset($event) ? $event->harga_satuan_single_bed_text : '') }}">
                                @if($errors->has('harga_satuan_single_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('harga_satuan_single_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.harga_satuan_single_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_harga_single_bed') ? 'has-error' : '' }}">
                                <label for="jumlah_harga_single_bed">{{ trans('global.event.fields.jumlah_harga_single_bed') }}</label>
                                <input type="text" id="jumlah_harga_single_bed" name="jumlah_harga_single_bed" class="form-control" value="{{ old('jumlah_harga_single_bed', isset($event) ? $event->jumlah_harga_single_bed : '') }}" readonly>
                                @if($errors->has('jumlah_harga_single_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_harga_single_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_harga_single_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}
                </div>
            </div>
            {{-- end single bed card --}}

            {{-- twin bed card --}}
            <div class="card only-fullboard" style="display:none">
                <div class="card-header">
                    <h4>Perserta Twin Bed</h4>
                </div>

                <div class="card-body">
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_peserta_twin_bed') ? 'has-error' : '' }}">
                                <label for="jumlah_peserta_twin_bed">{{ trans('global.event.fields.jumlah_peserta_twin_bed') }}</label>
                                <input type="number" id="jumlah_peserta_twin_bed" name="jumlah_peserta_twin_bed" class="form-control" value="{{ old('jumlah_peserta_twin_bed', isset($event) ? $event->jumlah_peserta_twin_bed : '') }}">
                                @if($errors->has('jumlah_peserta_twin_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_peserta_twin_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_peserta_twin_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_hari_peserta_twin_bed') ? 'has-error' : '' }}">
                                <label for="jumlah_hari_peserta_twin_bed">{{ trans('global.event.fields.jumlah_hari_peserta_twin_bed') }}</label>
                                <input type="number" id="jumlah_hari_peserta_twin_bed" name="jumlah_hari_peserta_twin_bed" class="form-control" value="{{ old('jumlah_hari_peserta_twin_bed', isset($event) ? $event->jumlah_hari_peserta_twin_bed : 1) }}" readonly>
                                @if($errors->has('jumlah_hari_peserta_twin_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_hari_peserta_twin_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_hari_peserta_twin_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}

                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('harga_satuan_twin_bed') ? 'has-error' : '' }}">
                                <label for="harga_satuan_twin_bed">{{ trans('global.event.fields.harga_satuan_twin_bed') }}</label>
                                <input type="number" id="harga_satuan_twin_bed" name="harga_satuan_twin_bed" class="form-control" value="{{ old('harga_satuan_twin_bed', isset($event) ? $event->harga_satuan_twin_bed : '') }}">
                                <input type="hidden" id="harga_satuan_twin_bed_text" name="harga_satuan_twin_bed_text" class="form-control" value="{{ old('harga_satuan_twin_bed_text', isset($event) ? $event->harga_satuan_twin_bed_text : '') }}">
                                @if($errors->has('harga_satuan_twin_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('harga_satuan_twin_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.harga_satuan_twin_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_harga_twin_bed') ? 'has-error' : '' }}">
                                <label for="jumlah_harga_twin_bed">{{ trans('global.event.fields.jumlah_harga_twin_bed') }}</label>
                                <input type="text" id="jumlah_harga_twin_bed" name="jumlah_harga_twin_bed" class="form-control" value="{{ old('jumlah_harga_twin_bed', isset($event) ? $event->jumlah_harga_twin_bed : '') }}" readonly>
                                @if($errors->has('jumlah_harga_twin_bed'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_harga_twin_bed') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_harga_twin_bed_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}
                </div>
            </div>
            {{-- end twin bed card --}}

            {{-- perserta card --}}
            <div class="card non-fullboard" style="display:none">
                <div class="card-header">
                    <h4>Peserta</h4>
                </div>

                <div class="card-body">
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_peserta') ? 'has-error' : '' }}">
                                <label for="jumlah_peserta">{{ trans('global.event.fields.jumlah_peserta') }}</label>
                                <input type="number" id="jumlah_peserta" name="jumlah_peserta" class="form-control" value="{{ old('jumlah_peserta', isset($event) ? $event->jumlah_peserta : '') }}">
                                @if($errors->has('jumlah_peserta'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_peserta') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_peserta_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_hari_peserta') ? 'has-error' : '' }}">
                                <label for="jumlah_hari_peserta">{{ trans('global.event.fields.jumlah_hari_peserta') }}</label>
                                <input type="number" id="jumlah_hari_peserta" name="jumlah_hari_peserta" class="form-control" value="{{ old('jumlah_hari_peserta', isset($event) ? $event->jumlah_hari_peserta : 1) }}" readonly>
                                @if($errors->has('jumlah_hari_peserta'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_hari_peserta') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_hari_peserta_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}

                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('harga_satuan') ? 'has-error' : '' }}">
                                <label for="harga_satuan">{{ trans('global.event.fields.harga_satuan') }}</label>
                                <input type="number" id="harga_satuan" name="harga_satuan" class="form-control" value="{{ old('harga_satuan', isset($event) ? $event->harga_satuan : "") }}">
                                <input type="hidden" id="harga_satuan_text" name="harga_satuan_text" class="form-control" value="{{ old('harga_satuan_text', isset($event) ? $event->harga_satuan_text : "") }}">
                                @if($errors->has('harga_satuan'))
                                    <p class="help-block">
                                        {{ $errors->first('harga_satuan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.harga_satuan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jumlah_harga') ? 'has-error' : '' }}">
                                <label for="jumlah_harga">{{ trans('global.event.fields.jumlah_harga') }}</label>
                                <input type="text" id="jumlah_harga" name="jumlah_harga" class="form-control" value="{{ old('jumlah_harga', isset($event) ? $event->jumlah_harga : '') }}" readonly>
                                @if($errors->has('jumlah_harga'))
                                    <p class="help-block">
                                        {{ $errors->first('jumlah_harga') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jumlah_harga_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}
                </div>
            </div>
            {{-- end perserta card --}}

            {{-- HPS card --}}
            <div class="card">
                <div class="card-header">
                    <h4>HPS</h4>
                </div>

                <div class="card-body">
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nilai_hps') ? 'has-error' : '' }}">
                                <label for="nilai_hps">{{ trans('global.event.fields.nilai_hps') }}</label>
                                <input type="text" id="nilai_hps" name="nilai_hps" class="form-control" value="{{ old('nilai_hps', isset($event) ? $event->nilai_hps : '') }}" readonly>
                                <input type="hidden" id="nilai_hps_text" name="nilai_hps_text" class="form-control" value="{{ old('nilai_hps_text', isset($event) ? $event->nilai_hps_text : '') }}" readonly>
                                @if($errors->has('nilai_hps'))
                                    <p class="help-block">
                                        {{ $errors->first('nilai_hps') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nilai_hps_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nilai_hps_terbilang') ? 'has-error' : '' }}">
                                <label for="nilai_hps_terbilang">{{ trans('global.event.fields.nilai_hps_terbilang') }}</label>
                                <input type="text" id="nilai_hps_terbilang" name="nilai_hps_terbilang" class="form-control" value="{{ old('nilai_hps_terbilang', isset($event) ? $event->nilai_hps_terbilang : '') }}" readonly>
                                @if($errors->has('nilai_hps_terbilang'))
                                    <p class="help-block">
                                        {{ $errors->first('nilai_hps_terbilang') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nilai_hps_terbilang_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}

                </div>
            </div>
            {{-- end HPS card --}}

            {{-- Perusahaan card --}}
            <div class="card">
                <div class="card-header">
                    <h4>Hotel & Perusahaan</h4>
                </div>

                <div class="card-body">
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nama_hotel') ? 'has-error' : '' }}">
                                <label for="nama_hotel">{{ trans('global.event.fields.nama_hotel') }}</label>
                                <input type="text" id="nama_hotel" name="nama_hotel" class="form-control" value="{{ old('nama_hotel', isset($event) ? $event->nama_hotel : '') }}">
                                @if($errors->has('nama_hotel'))
                                    <p class="help-block">
                                        {{ $errors->first('nama_hotel') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nama_hotel_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-12 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('alamat_hotel') ? 'has-error' : '' }}">
                                <label for="alamat_hotel">{{ trans('global.event.fields.alamat_hotel') }}</label>
                                <textarea id="alamat_hotel" name="alamat_hotel" class="form-control ">{{ old('alamat_hotel', isset($event) ? $event->alamat_hotel : '')  }}</textarea>
                                @if($errors->has('alamat_hotel'))
                                    <p class="help-block">
                                        {{ $errors->first('alamat_hotel') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.alamat_hotel_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nama_perusahaan') ? 'has-error' : '' }}">
                                <label for="nama_perusahaan">{{ trans('global.event.fields.nama_perusahaan') }}</label>
                                <input type="text" id="nama_perusahaan" name="nama_perusahaan" class="form-control" value="{{ old('nama_perusahaan', isset($event) ? $event->nama_perusahaan : '') }}">
                                @if($errors->has('nama_perusahaan'))
                                    <p class="help-block">
                                        {{ $errors->first('nama_perusahaan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nama_perusahaan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('no_npwp_perusahaan') ? 'has-error' : '' }}">
                                <label for="no_npwp_perusahaan">{{ trans('global.event.fields.no_npwp_perusahaan') }}</label>
                                <input type="text" id="no_npwp_perusahaan" name="no_npwp_perusahaan" class="form-control" value="{{ old('no_npwp_perusahaan', isset($event) ? $event->no_npwp_perusahaan : '') }}">
                                @if($errors->has('no_npwp_perusahaan'))
                                    <p class="help-block">
                                        {{ $errors->first('no_npwp_perusahaan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.no_npwp_perusahaan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                    </div>
                    {{-- end row --}}

                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nama_pejabat_hotel') ? 'has-error' : '' }}">
                                <label for="nama_pejabat_hotel">{{ trans('global.event.fields.nama_pejabat_hotel') }}</label>
                                <input type="text" id="nama_pejabat_hotel" name="nama_pejabat_hotel" class="form-control" value="{{ old('nama_pejabat_hotel', isset($event) ? $event->nama_pejabat_hotel : '') }}">
                                @if($errors->has('nama_pejabat_hotel'))
                                    <p class="help-block">
                                        {{ $errors->first('nama_pejabat_hotel') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nama_pejabat_hotel_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-6 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('jabatan_pejabat_hotel') ? 'has-error' : '' }}">
                                <label for="jabatan_pejabat_hotel">{{ trans('global.event.fields.jabatan_pejabat_hotel') }}</label>
                                <input type="text" id="jabatan_pejabat_hotel" name="jabatan_pejabat_hotel" class="form-control" value="{{ old('jabatan_pejabat_hotel', isset($event) ? $event->jabatan_pejabat_hotel : '') }}">
                                @if($errors->has('jabatan_pejabat_hotel'))
                                    <p class="help-block">
                                        {{ $errors->first('jabatan_pejabat_hotel') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.jabatan_pejabat_hotel_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}

                    </div>
                    {{-- end row --}}
                    {{-- row --}}
                    <div class="row">
                        {{-- field --}}
                        <div class="col-lg-4 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nama_bank_perusahaan') ? 'has-error' : '' }}">
                                <label for="nama_bank_perusahaan">{{ trans('global.event.fields.nama_bank_perusahaan') }}</label>
                                <input type="text" id="nama_bank_perusahaan" name="nama_bank_perusahaan" class="form-control" value="{{ old('nama_bank_perusahaan', isset($event) ? $event->nama_bank_perusahaan : '') }}">
                                @if($errors->has('nama_bank_perusahaan'))
                                    <p class="help-block">
                                        {{ $errors->first('nama_bank_perusahaan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nama_bank_perusahaan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-4 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('no_rekening_bank_perusahaan') ? 'has-error' : '' }}">
                                <label for="no_rekening_bank_perusahaan">{{ trans('global.event.fields.no_rekening_bank_perusahaan') }}</label>
                                <input type="text" id="no_rekening_bank_perusahaan" name="no_rekening_bank_perusahaan" class="form-control" value="{{ old('no_rekening_bank_perusahaan', isset($event) ? $event->no_rekening_bank_perusahaan : '') }}">
                                @if($errors->has('no_rekening_bank_perusahaan'))
                                    <p class="help-block">
                                        {{ $errors->first('no_rekening_bank_perusahaan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.no_rekening_bank_perusahaan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}
                        {{-- field --}}
                        <div class="col-lg-4 col-sm-12 col-md-12">
                            <div class="form-group {{ $errors->has('nama_pemilik_rekening_bank_perusahaan') ? 'has-error' : '' }}">
                                <label for="nama_pemilik_rekening_bank_perusahaan">{{ trans('global.event.fields.nama_pemilik_rekening_bank_perusahaan') }}</label>
                                <input type="text" id="nama_pemilik_rekening_bank_perusahaan" name="nama_pemilik_rekening_bank_perusahaan" class="form-control" value="{{ old('nama_pemilik_rekening_bank_perusahaan', isset($event) ? $event->nama_pemilik_rekening_bank_perusahaan : '') }}">
                                @if($errors->has('nama_pemilik_rekening_bank_perusahaan'))
                                    <p class="help-block">
                                        {{ $errors->first('nama_pemilik_rekening_bank_perusahaan') }}
                                    </p>
                                @endif
                                <p class="helper-block">
                                    {{ trans('global.event.fields.nama_pemilik_rekening_bank_perusahaan_helper') }}
                                </p>
                            </div>
                        </div>
                        {{-- end field --}}

                    </div>
                    {{-- end row --}}
                    
                </div>
            </div>
            {{-- end Perusahaan card --}}

            <div class="card">
                <div class="card-footer">
                    <div>
                        <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@section('scripts')
@parent
<script>
var newEms;
var eventEditPageScript = {
    eventType: "<?php echo $event->tipe_pekerjaan; ?>", //initial value
    nilaiHPS:0,
    today:"<?php echo date('Y-m-d'); ?>",
    tommorow:"<?php $date = strtotime("+1 day"); echo date('Y-m-d', $date); ?>",
    eventDays:1,
    displayNilaiHPS:function(){
        return accounting.formatMoney(eventEditPageScript.nilaiHPS, "Rp. ",2, ".", ",");
    },
    displayNilaiHPSTerbilang:function(){
        return newEms.formatMoneyToText(eventEditPageScript.nilaiHPS, "rupiah");
    },
    resetCalculation: function(){
        let $tanggalMulai = $(document).find("#tanggal_mulai");
        let $tanggalSelesai = $(document).find("#tanggal_selesai");
        $tanggalMulai.val("<?php echo $event->tanggal_mulai ?>");
        $tanggalSelesai.val("<?php echo $event->tanggal_selesai ?>");
        
        // fullday / halfday
        let $jumlalhPeserta = $(document).find("#jumlah_peserta");
        let $jumlahHari = $(document).find("#jumlah_hari_peserta");
        let $hargaSatuan = $(document).find("#harga_satuan");
        let $jumlahHarga = $(document).find("#jumlah_harga");
        $jumlalhPeserta.val("<?php echo $event->jumlah_peserta;?>");
        $jumlahHari.val("<?php echo $event->jumlah_hari_peserta;?>");
        $hargaSatuan.val("<?php echo $event->harga_satuan;?>");
        $jumlahHarga.val("");

        // fullboard
        // single
        let $jumlalhPesertaSingleBed = $(document).find("#jumlah_peserta_single_bed");
        let $jumlahHariSingleBed = $(document).find("#jumlah_hari_peserta_single_bed");
        let $hargaSatuanSingleBed = $(document).find("#harga_satuan_single_bed");
        let $jumlahHargaSingleBed = $(document).find("#jumlah_harga_single_bed");
        $jumlalhPesertaSingleBed.val("<?php echo $event->jumlah_peserta_single_bed;?>");
        $jumlahHariSingleBed.val("<?php echo $event->jumlah_hari_peserta_single_bed;?>");
        $hargaSatuanSingleBed.val("<?php echo $event->harga_satuan_single_bed;?>");
        $jumlahHargaSingleBed.val("");
        // twin
        let $jumlalhPesertaTwinBed = $(document).find("#jumlah_peserta_twin_bed");
        let $jumlahHariTwinBed = $(document).find("#jumlah_hari_peserta_twin_bed");
        let $hargaSatuanTwinBed = $(document).find("#harga_satuan_twin_bed");
        let $jumlahHargaTwinBed = $(document).find("#jumlah_harga_twin_bed");
        $jumlalhPesertaTwinBed.val("<?php echo $event->jumlah_peserta_twin_bed;?>");
        $jumlahHariTwinBed.val("<?php echo $event->jumlah_hari_peserta_twin_bed;?>");
        $hargaSatuanTwinBed.val("<?php echo $event->harga_satuan_twin_bed;?>");
        $jumlahHargaTwinBed.val("");
        // general
        eventEditPageScript.nilaiHPS = 0;
        $(document).find("#nilai_hps").val(eventEditPageScript.displayNilaiHPS());
        $(document).find("#nilai_hps_terbilang").val(eventEditPageScript.displayNilaiHPSTerbilang());
    },
    calculateHPS: function(){
        if(eventEditPageScript.eventType == "fullboard"){
            // for fullboard
            // single
            let $jumlalhPesertaSingleBed = $(document).find("#jumlah_peserta_single_bed");
            let $jumlahHariSingleBed = $(document).find("#jumlah_hari_peserta_single_bed");
            let $hargaSatuanSingleBed = $(document).find("#harga_satuan_single_bed");
            let $hargaSatuanSingleBedText = $(document).find("#harga_satuan_single_bed_text");
            let $jumlahHargaSingleBed = $(document).find("#jumlah_harga_single_bed");
            let totalSingleBed = parseInt($jumlalhPesertaSingleBed.val() || 0) * parseInt($jumlahHariSingleBed.val() || 0) * parseFloat($hargaSatuanSingleBed.val() || 0) ;
            $hargaSatuanSingleBedText.val(accounting.formatMoney($hargaSatuanSingleBed.val() || 0, "", 2, ".", ","));
            $jumlahHargaSingleBed.val(accounting.formatMoney(totalSingleBed, "Rp. ", 2, ".", ","));

            // twin
            let $jumlalhPesertaTwinBed = $(document).find("#jumlah_peserta_twin_bed");
            let $jumlahHariTwinBed = $(document).find("#jumlah_hari_peserta_twin_bed");
            let $hargaSatuanTwinBed = $(document).find("#harga_satuan_twin_bed");
            let $hargaSatuanTwinBedText = $(document).find("#harga_satuan_twin_bed_text");
            let $jumlahHargaTwinBed = $(document).find("#jumlah_harga_twin_bed");
            let totalTwinBed = parseInt($jumlalhPesertaTwinBed.val() || 0) * parseInt($jumlahHariTwinBed.val() || 0) * parseFloat($hargaSatuanTwinBed.val() || 0) ;
            $hargaSatuanTwinBedText.val(accounting.formatMoney($hargaSatuanTwinBed.val() || 0, "", 2, ".", ","));
            $jumlahHargaTwinBed.val(accounting.formatMoney(totalTwinBed, "Rp. ", 2, ".", ","));

            // total single + twin bed
            let total = totalSingleBed + totalTwinBed;
            eventEditPageScript.nilaiHPS = total;
        }else{
            let $jumlalhPeserta = $(document).find("#jumlah_peserta");
            let $jumlahHari = $(document).find("#jumlah_hari_peserta");
            let $hargaSatuan = $(document).find("#harga_satuan");
            let $hargaSatuanText = $(document).find("#harga_satuan_text");
            let $jumlahHarga = $(document).find("#jumlah_harga");
            let total = parseInt($jumlalhPeserta.val()) * parseInt($jumlahHari.val()) * parseFloat($hargaSatuan.val()) ;
            $hargaSatuanText.val(accounting.formatMoney($hargaSatuan.val() || 0, "", 2, ".", ","));
            $jumlahHarga.val(accounting.formatMoney(total, "Rp. ", 2, ".", ","));

            eventEditPageScript.nilaiHPS = total;
        }

        $(document).find("#nilai_hps").val(eventEditPageScript.displayNilaiHPS());
        $(document).find("#nilai_hps_text").val(accounting.formatMoney(eventEditPageScript.nilaiHPS, "",2, ".", ","));
        $(document).find("#nilai_hps_terbilang").val(eventEditPageScript.displayNilaiHPSTerbilang());
    },
    onChangeEventType(val){
        eventEditPageScript.resetCalculation();
        eventEditPageScript.calculateHPS();
        if(val == "fullboard") {
            $(document).find(".only-fullboard").show();
            $(document).find(".non-fullboard").hide();
        }else{
            $(document).find(".only-fullboard").hide();
            $(document).find(".non-fullboard").show();
        }
        eventEditPageScript.eventType = val;

        eventEditPageScript.calculateHPS();
    },
    onChangeEventDate(){
        let startDate = $(document).find("#tanggal_mulai").val();
        let endDate = $(document).find("#tanggal_selesai").val();

        console.log(startDate, endDate);

        let diff = newEms.dateDiff(startDate, endDate);

        console.log(diff);

        if(diff < 1 && eventEditPageScript.eventType == "fullboard"){
            alert("Mohon set tanggal mulai sebelum tanggal selesai");
            $(document).find("#tanggal_mulai").val(eventEditPageScript.today);
            $(document).find("#tanggal_selesai").val(eventEditPageScript.tommorow);
            diff = 1;
        }

        if(eventEditPageScript.eventType != "fullboard"){
            diff = 1;
        }

        eventEditPageScript.onChangeEventDays(diff);

    },
    onChangeEventDays: function(val){
        eventEditPageScript.eventDays = val;

        if(eventEditPageScript.eventType == "fullboard"){
            $(document).find("#jumlah_hari_peserta_single_bed").val(eventEditPageScript.eventDays);
            $(document).find("#jumlah_hari_peserta_twin_bed").val(eventEditPageScript.eventDays);
        }else{
            $(document).find("#jumlah_hari_peserta").val(eventEditPageScript.eventDays);
        }
        eventEditPageScript.calculateHPS();
    },
    init: function(){
         // init 
        $("#tipe_pekerjaan").select2({
            "placeholder":"Fullday/Halfday/Fullboard"
        });

        $("#tipe_pekerjaan").on("change", function(){
            let val = $(this).val();
            eventEditPageScript.onChangeEventType(val)
        });

        $("#tipe_pekerjaan").val(eventEditPageScript.eventType).trigger("change");


        $(document).on("change", "#tanggal_mulai,#tanggal_selesai", function(){
            eventEditPageScript.onChangeEventDate();
        })

        // init on change for HPS
        // fullday/halfday
        $(document).find("#jumlah_peserta,#jumlah_hari_peserta,#harga_satuan").on("change", function(){
            eventEditPageScript.calculateHPS()
        });
        // fullboard
        $(document).find("#jumlah_peserta_single_bed,#jumlah_hari_peserta_single_bed,#harga_satuan_single_bed,#jumlah_peserta_twin_bed,#jumlah_hari_peserta_twin_bed,#harga_satuan_twin_bed").on("change", function(){
            eventEditPageScript.calculateHPS()
        });

        // hps section
        $(document).find("#nilai_hps").val(accounting.formatMoney(eventEditPageScript.nilaiHPS, "Rp. ",2, ".", ","))
        $(document).on("change", "#nilai_hps", function(e){
            let val = $(this).val();
            
            $(this).val(accounting.formatMoney(val, "Rp.", ".", ","));
        })
    }
}


$(function () {
    newEms = clone(ems)
    eventEditPageScript.init();
})

</script>
@endsection