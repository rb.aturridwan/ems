@extends('layouts.admin')
@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ trans('global.event.title_singular') }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @isset($breadcrumbs)
                    @foreach($breadcrumbs as $key => $breadcrumb )
                        @if($key == count($breadcrumbs)-1)
                        <li class="breadcrumb-item active">{{$breadcrumb}}</li>
                        @else
                        <li class="breadcrumb-item">{{$breadcrumb}}</li>
                        @endif
                    @endforeach
                    @endisset
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
@endsection
<ul>
@isset($errors)
@foreach($errors as $error)
        <li>$error</li>
@endforeach
@endisset
</ul>
@section('content')
<form action="{{ route("admin.events.documents.store", [$event->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="event_id" value="{{$event->id}}" />
    <input type="hidden" name="type" value="cover" />
    {{-- main card --}}
    <div class="card">
        <div class="card-header">
            <h4>Cover</h4>
        </div>

        <div class="card-body">
            {{-- row --}}
            <div class="row">
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <span class="font-italic text-black-50"> Klik buat dokument untuk melanjutkan </span>
                </div>

                {{-- field --}}
                {{-- <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('no_surat') ? 'has-error' : '' }}">
                        <label for="no_surat">{{ trans('global.event.documents.fields.no_surat') }}<span class="font-italic text-black-50"> (optional)</span></label>
                        <input type="text" id="no_surat" name="no_surat" class="form-control" value="{{ old('no_surat', isset($event) ? $event->no_surat : '') }}">
                        @if($errors->has('no_surat'))
                            <p class="help-block">
                                {{ $errors->first('no_surat') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.no_surat_helper') }}
                        </p>
                    </div>
                </div> --}}
                {{-- end field --}}
            </div>
            {{-- end row --}}
        </div>

        <div class="card-footer">
            <div>
                <input class="btn btn-danger" type="submit" value="Buat Dokumen">
            </div>
        </div>
    </div>
</form>
@endsection
@section('scripts')
@parent
<script>
var newEms;
var createDocumentPageScript = {
    init: function(){

    }
}


$(function () {
    newEms = clone(ems)
    createDocumentPageScript.init();
})

</script>
@endsection