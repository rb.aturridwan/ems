@extends('layouts.admin')
@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ trans('global.event.title_singular') }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @isset($breadcrumbs)
                    @foreach($breadcrumbs as $key => $breadcrumb )
                        @if($key == count($breadcrumbs)-1)
                        <li class="breadcrumb-item active">{{$breadcrumb}}</li>
                        @else
                        <li class="breadcrumb-item">{{$breadcrumb}}</li>
                        @endif
                    @endforeach
                    @endisset
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
@endsection
<ul>
@isset($errors)
@foreach($errors as $error)
        <li>$error</li>
@endforeach
@endisset
</ul>
@section('content')
<form action="{{ route("admin.events.documents.store", [$event->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="event_id" value="{{$event->id}}" />
    <input type="hidden" name="type" value="undanganpenyedia" />
    {{-- main card --}}
    <div class="card">
        <div class="card-header">
            <h4>Undangan Ke Penyedia</h4>
        </div>
        <div class="card-body">
            {{-- row --}}
            <div class="row">

                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('tanggal') ? 'has-error' : '' }}">
                        <label for="tanggal">{{ trans('global.event.documents.fields.tanggal') }}<span class="text-danger">*</span></label>
                        <input type="date" id="tanggal" name="tanggal" class="form-control" value="{{ old('tanggal', isset($document) ? $document->tanggal : '') }}" required>
                        @if($errors->has('tanggal'))
                            <p class="help-block">
                                {{ $errors->first('tanggal') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.tanggal_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('no_surat') ? 'has-error' : '' }}">
                        <label for="no_surat">{{ trans('global.event.documents.fields.no_surat') }}<span class="text-danger">*</span></label>
                        <input type="text" id="no_surat" name="no_surat" class="form-control" value="{{ old('no_surat', isset($document) ? $document->no_surat : '') }}" required>
                        @if($errors->has('no_surat'))
                            <p class="help-block">
                                {{ $errors->first('no_surat') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.no_surat_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('tanggal_pemasukan_dokumen_penawaran') ? 'has-error' : '' }}">
                        <label for="tanggal_pemasukan_dokumen_penawaran">{{ trans('global.event.documents.fields.tanggal_pemasukan_dokumen_penawaran') }}<span class="text-danger">*</span></label>
                        <input type="date" id="tanggal_pemasukan_dokumen_penawaran" name="tanggal_pemasukan_dokumen_penawaran" class="form-control" value="{{ old('tanggal_pemasukan_dokumen_penawaran', isset($document) && isset($document_data->tanggal_pemasukan_dokumen_penawaran) ? $document_data->tanggal_pemasukan_dokumen_penawaran : '') }}" required>
                        @if($errors->has('tanggal_pemasukan_dokumen_penawaran'))
                            <p class="help-block">
                                {{ $errors->first('tanggal_pemasukan_dokumen_penawaran') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.tanggal_pemasukan_dokumen_penawaran_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('waktu_pemasukan_dokumen_penawaran') ? 'has-error' : '' }}">
                        <label for="waktu_pemasukan_dokumen_penawaran">{{ trans('global.event.documents.fields.waktu_pemasukan_dokumen_penawaran') }}<span class="text-danger">*</span></label>
                        <input type="text" id="waktu_pemasukan_dokumen_penawaran" name="waktu_pemasukan_dokumen_penawaran" class="form-control" value="{{ old('waktu_pemasukan_dokumen_penawaran', isset($document) && isset($document_data->waktu_pemasukan_dokumen_penawaran) ? $document_data->waktu_pemasukan_dokumen_penawaran : '') }}" required placeholder="ex: 08.00 s.d 20.00 WIB">
                        @if($errors->has('waktu_pemasukan_dokumen_penawaran'))
                            <p class="help-block">
                                {{ $errors->first('waktu_pemasukan_dokumen_penawaran') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.waktu_pemasukan_dokumen_penawaran_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('tanggal_evaluasi_penawaran') ? 'has-error' : '' }}">
                        <label for="tanggal_evaluasi_penawaran">{{ trans('global.event.documents.fields.tanggal_evaluasi_penawaran') }}<span class="text-danger">*</span></label>
                        <input type="date" id="tanggal_evaluasi_penawaran" name="tanggal_evaluasi_penawaran" class="form-control" value="{{ old('tanggal_evaluasi_penawaran', isset($document) && isset($document_data->tanggal_evaluasi_penawaran) ? $document_data->tanggal_evaluasi_penawaran : '') }}" required>
                        @if($errors->has('tanggal_evaluasi_penawaran'))
                            <p class="help-block">
                                {{ $errors->first('tanggal_evaluasi_penawaran') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.tanggal_evaluasi_penawaran_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('waktu_evaluasi_penawaran') ? 'has-error' : '' }}">
                        <label for="waktu_evaluasi_penawaran">{{ trans('global.event.documents.fields.waktu_evaluasi_penawaran') }}<span class="text-danger">*</span></label>
                        <input type="text" id="waktu_evaluasi_penawaran" name="waktu_evaluasi_penawaran" class="form-control" value="{{ old('waktu_evaluasi_penawaran', isset($document) && isset($document_data->waktu_evaluasi_penawaran) ? $document_data->waktu_evaluasi_penawaran : '') }}" required placeholder="ex: 08.00 s.d 20.00 WIB">
                        @if($errors->has('waktu_evaluasi_penawaran'))
                            <p class="help-block">
                                {{ $errors->first('waktu_evaluasi_penawaran') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.waktu_evaluasi_penawaran_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('tanggal_spk') ? 'has-error' : '' }}">
                        <label for="tanggal_spk">{{ trans('global.event.documents.fields.tanggal_spk') }}<span class="text-danger">*</span></label>
                        <input type="date" id="tanggal_spk" name="tanggal_spk" class="form-control" value="{{ old('tanggal_spk', isset($document) && isset($document_data->tanggal_spk) ? $document_data->tanggal_spk : '') }}" required>
                        @if($errors->has('tanggal_spk'))
                            <p class="help-block">
                                {{ $errors->first('tanggal_spk') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.tanggal_spk_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('tanggal_spmk') ? 'has-error' : '' }}">
                        <label for="tanggal_spmk">{{ trans('global.event.documents.fields.tanggal_spmk') }}<span class="text-danger">*</span></label>
                        <input type="date" id="tanggal_spmk" name="tanggal_spmk" class="form-control" value="{{ old('tanggal_spmk', isset($document) && isset($document_data->tanggal_spmk) ? $document_data->tanggal_spmk : '') }}" required>
                        @if($errors->has('tanggal_spmk'))
                            <p class="help-block">
                                {{ $errors->first('tanggal_spmk') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.tanggal_spmk_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
            </div>
            {{-- end row --}}
        </div>

        <div class="card-footer">
            <div>
                <input class="btn btn-danger" type="submit" value="Buat Dokumen">
            </div>
        </div>
    </div>
</form>
@endsection
@section('scripts')
@parent
<script>
var newEms;
var createDocumentPageScript = {
    init: function(){

    }
}


$(function () {
    newEms = clone(ems)
    createDocumentPageScript.init();
})

</script>
@endsection