@extends('layouts.admin')
@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ trans('global.event.title_singular') }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @isset($breadcrumbs)
                    @foreach($breadcrumbs as $key => $breadcrumb )
                        @if($key == count($breadcrumbs)-1)
                        <li class="breadcrumb-item active">{{$breadcrumb}}</li>
                        @else
                        <li class="breadcrumb-item">{{$breadcrumb}}</li>
                        @endif
                    @endforeach
                    @endisset
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
@endsection
<ul>
@isset($errors)
@foreach($errors as $error)
        <li>$error</li>
@endforeach
@endisset
</ul>
@section('content')
<form action="{{ route("admin.events.documents.store", [$event->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="event_id" value="{{$event->id}}" />
    <input type="hidden" name="type" value="hps" />
    {{-- main card --}}
    <div class="card">
        <div class="card-header">
            <h4>HPS</h4>
        </div>

        <div class="card-body">
            {{-- row --}}
            <div class="row">

                {{-- field --}}
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="form-group {{ $errors->has('tanggal_hps') ? 'has-error' : '' }}">
                        <label for="tanggal_hps">{{ trans('global.event.documents.fields.tanggal_hps') }}<span class="text-danger">*</span></label>
                        <input type="date" id="tanggal_hps" name="tanggal_hps" class="form-control" value="{{ old('tanggal_hps', isset($document) ? $document->tanggal : '') }}" required>
                        @if($errors->has('tanggal_hps'))
                            <p class="help-block">
                                {{ $errors->first('tanggal_hps') }}
                            </p>
                        @endif
                        <p class="helper-block">
                            {{ trans('global.event.documents.fields.tanggal_hps_helper') }}
                        </p>
                    </div>
                </div>
                {{-- end field --}}
            </div>
            {{-- end row --}}
        </div>

        <div class="card-footer">
            <div>
                <input class="btn btn-danger" type="submit" value="Buat Dokumen">
            </div>
        </div>
    </div>
</form>
@endsection
@section('scripts')
@parent
<script>
var newEms;
var createDocumentPageScript = {
    init: function(){

    }
}


$(function () {
    newEms = clone(ems)
    createDocumentPageScript.init();
})

</script>
@endsection