@extends('layouts.admin')
@section('breadcrumb')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">{{ trans('global.event.title_singular') }}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    @isset($breadcrumbs)
                    @foreach($breadcrumbs as $key => $breadcrumb )
                        @if($key == count($breadcrumbs)-1)
                        <li class="breadcrumb-item active">{{$breadcrumb}}</li>
                        @else
                        <li class="breadcrumb-item">{{$breadcrumb}}</li>
                        @endif
                    @endforeach
                    @endisset
                </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
        </div>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>{{ trans('global.show') }} {{ trans('global.event.title') }}</h5>
                </div>
            
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th width="30%">
                                    {{ trans('global.event.fields.nama_kegiatan') }}
                                </th>
                                <td>
                                    {{ $event->nama_kegiatan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.tipe_pekerjaan') }}
                                </th>
                                <td>
                                    {!! $event->tipe_pekerjaan !!}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.kode_mak') }}
                                </th>
                                <td>
                                    {{ $event->kode_mak }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.daerah_kegiatan') }}
                                </th>
                                <td>
                                    {{ $event->daerah_kegiatan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.tanggal_mulai') }}
                                </th>
                                <td>
                                    {{ $event->tanggal_mulai }}
                                </td>
                            </tr>
                            @if($event->tipe_pekerjaan == "fullboard")
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.tanggal_selesai') }}
                                </th>
                                <td>
                                    {{ $event->tanggal_selesai }}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>HPS</h5>
                </div>
            
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th width="30%">
                                    {{ trans('global.event.fields.nilai_hps') }}
                                </th>
                                <td>
                                    {{ $event->nilai_hps }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.nilai_hps_terbilang') }}
                                </th>
                                <td>
                                    {!! $event->nilai_hps_terbilang !!}
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if($event->tipe_pekerjaan != "fullboard")
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Peserta</h5>
                </div>
            
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th width="30%">
                                    {{ trans('global.event.fields.jumlah_peserta') }}
                                </th>
                                <td>
                                    {{ $event->jumlah_peserta }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.jumlah_hari_peserta') }}
                                </th>
                                <td>
                                    {!! $event->jumlah_hari_peserta !!}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.harga_satuan') }}
                                </th>
                                <td>
                                    {{ $event->harga_satuan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.jumlah_harga') }}
                                </th>
                                <td>
                                    {{ $event->jumlah_harga }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @else
            @if($event->jumlah_peserta_single_bed > 0)
                @if($event->jumlah_peserta_single_bed > 0 && $event->jumlah_peserta_twin_bed > 0)
                <div class="col-6">
                @else
                <div class="col-12">
                @endif
                    <div class="card">
                        <div class="card-header">
                            <h5>Peserta Single Bed</h5>
                        </div>
                
                        <div class="card-body">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th width="30%">
                                            {{ trans('global.event.fields.jumlah_peserta') }}
                                        </th>
                                        <td>
                                            {{ $event->jumlah_peserta_single_bed }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('global.event.fields.jumlah_hari_peserta') }}
                                        </th>
                                        <td>
                                            {!! $event->jumlah_hari_peserta_single_bed !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('global.event.fields.harga_satuan') }}
                                        </th>
                                        <td>
                                            {{ $event->harga_satuan_single_bed }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('global.event.fields.jumlah_harga') }}
                                        </th>
                                        <td>
                                            {{ $event->jumlah_harga_single_bed }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
            
            @if($event->jumlah_peserta_twin_bed > 0)
                @if($event->jumlah_peserta_single_bed > 0 && $event->jumlah_peserta_twin_bed > 0)
                <div class="col-6">
                @else
                <div class="col-12">
                @endif
                    <div class="card">
                        <div class="card-header">
                            <h5>Peserta Twin Bed</h5>
                        </div>
                
                        <div class="card-body">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                    <tr>
                                        <th width="30%">
                                            {{ trans('global.event.fields.jumlah_peserta') }}
                                        </th>
                                        <td>
                                            {{ $event->jumlah_peserta_twin_bed }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('global.event.fields.jumlah_hari_peserta') }}
                                        </th>
                                        <td>
                                            {!! $event->jumlah_hari_peserta_twin_bed !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('global.event.fields.harga_satuan') }}
                                        </th>
                                        <td>
                                            {{ $event->harga_satuan_twin_bed }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            {{ trans('global.event.fields.jumlah_harga') }}
                                        </th>
                                        <td>
                                            {{ $event->jumlah_harga_twin_bed }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h5>Hotel & Perusahaan</h5>
                </div>
            
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th width="30%">
                                    {{ trans('global.event.fields.nama_hotel') }}
                                </th>
                                <td>
                                    {{ $event->nama_hotel }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.alamat_hotel') }}
                                </th>
                                <td>
                                    {!! $event->alamat_hotel !!}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.nama_perusahaan') }}
                                </th>
                                <td>
                                    {{ $event->nama_perusahaan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.no_npwp_perusahaan') }}
                                </th>
                                <td>
                                    {{ $event->no_npwp_perusahaan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.nama_pejabat_hotel') }}
                                </th>
                                <td>
                                    {{ $event->nama_pejabat_hotel }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.jabatan_pejabat_hotel') }}
                                </th>
                                <td>
                                    {{ $event->jabatan_pejabat_hotel }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.nama_bank_perusahaan') }}
                                </th>
                                <td>
                                    {{ $event->nama_bank_perusahaan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.no_rekening_bank_perusahaan') }}
                                </th>
                                <td>
                                    {{ $event->no_rekening_bank_perusahaan }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('global.event.fields.nama_pemilik_rekening_bank_perusahaan') }}
                                </th>
                                <td>
                                    {{ $event->nama_pemilik_rekening_bank_perusahaan }}
                                </td>
                            </tr>
                       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection