<?php

Route::redirect('/', '/login');

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');

    Route::delete('events/destroy', 'EventsController@massDestroy')->name('events.massDestroy');

    Route::resource('events', 'EventsController');

    Route::get('events/{id}/documents', 'EventsController@documents')->name('events.documents');
    Route::get('events/{id}/documents/{type}/create', 'EventsController@createDocument')->name('events.documents.create');
    Route::post('events/{id}/documents', 'EventsController@storeDocument')->name('events.documents.store');

});
